# pygeodyn_data

This repository contains datasets that are to be used by the [pygeodyn package](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn), a Python package for geomagnetic data assimilation.

## Data types
 We only list here the available datasets. For more information on data types and on their use in pygeodyn, see the [documentation of pygeodyn](https://geodynamo.gricad-pages.univ-grenoble-alpes.fr/pygeodyn/index.html).

## Priors
* **0path**: 1505 samples of magnetic field (`RealB.dat`), subgrid error (`RealER.dat`) and core flow (`RealU.dat`) Gauss coefficients. These are sampled from a Coupled-Earth geodynamo free run [*AFF13*].
* **50path**:
    101037 samples of magnetic field (`RealB.dat`), subgrid error (`RealER.dat`) and core flow (`RealU.dat`) Gauss coefficients. These are sampled from a Midpath geodynamo free run [*AGF13*].
    For more efficient storing and access, a hdf5 file containing all the data (`times`, `MF`, `U`, `ER`) is also supplied (`Real.hdf5`).
* **71path**:
    A hdf5 file containing 50001 snapshots at different times of the magnetic field, the subgrid error and core flow Gauss coefficients (keys are in order `times`, `MF`, `ER` and `U`) . These are sampled from a `71path` geodynamo free run [*AG21*]. The `71-path` dynamo uses parameters that are closer to Earth's parameter than 'midpath' and is therefore a more realistic prior.
* **100path**:
    A set of 8 sampled independant 100path geodynamo runs stored in 8 separate hdf5 files each containing `times`, `MF`, `ER` and `U`.[*Aubert23*]. These time series provide high time resolution but are short in time. Therefore, this prior requires to be used along with a prior that has a lower time resoluton but longer time series (such as *71path*).
* **S1fs**:
    A hdf5 file containing 15328 snapshots at different times of the magnetic field, the subgrid error and core flow Gauss coefficients (keys are in order `times`, `MF`, `ER` and `U`) . These are sampled from a `S1*` geodynamo free run.

## Observations

* **CHAOS-7**: Model of the time-dependent near-Earth geomagnetic field based on magnetic field observations collected by the low-Earth orbit satellites Swarm, CryoSat-2, CHAMP, SAC-C and Oersted, and on annual differences of monthly means of ground observatory measurements [*FKO20*]. 

* **COVOBS-x2_maglat**: Model submitted to the IGRF special issue of Earth, Planets and Space. Improvement of the COVOBS-x2_igrf model with different treatement of satellite data at high magnetic latitudes (hence `maglat`...). Provided as a HDF5 file that contains a dataset containing the dates (`times`), the Gauss coefficients of the magnetic field (`gnm`) and of the secular variation (`dgnm`). Can be loaded in pygeodyn with `COVOBS_hdf5` format.

* **COVOBS-x2_400reals**: Enabling up to 400 realisations. Same format as `COVOBS-x2_maglat`.

* **GOVO**: Magnetic observation files of the ground observatories (`GObs*.cdf`), the satellites Oersted (`OR*.cdf`) , Champ (`CH*.cdf`) , Cryosat (`CR*.cdf`) and Swarm (`SW*.cdf`), which covers the period 1997-2023. The satellite data is made of virtual observatories [*Hammer2021a*, *Hammer2021b*], sampled every 4 months for the Swarm satellites, and the other are sampled every 12 months due to insufficient local time coverage. Most recent files can be found [here](https://www.spacecenter.dk/files/magnetic-models/GVO/). cdf files can be read using the cdflib package [`cdflib`], an example function is in [read_observatories_cdf_data](https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pygeodyn/-/blob/master/pygeodyn/inout/reads.py#L168).

* **KALMAG**: Model proposed as a candidate for IGRF-13 [JMS22] 

* **COV-SAT**: Model submitted to the 14th IGRF call. Model based on the satellite period from 2000 to 2024 and forecasted from 2025 to 2030.

## References
* *Aubert23*
    Aubert, J.: State and evolution of the geodynamo from numerical models reaching the physical conditions of Earth’s core, Geophys. J. Int. 235, 468-487, 2023, doi: 10.1093/gji/ggad229

* *AG21*
	Aubert, J. & Gillet, N., 2021. The interplay of fast waves and slow convection in geodynamo simulations nearing earth’s core conditions, Geophys. J. Int., 225(3), 1854–1873.

* *AFF13*: 
    Aubert, J., Finlay, C. C. & Fournier, A. Bottom-up control of geomagnetic secular variation by the Earth’s inner core. *Nature* 502, 219–223 (2013). doi:10.1038/nature12574

* *AGF17*: 
    Aubert, J., Gastine, T. & Fournier, A. Spherical convective dynamos in the rapidly rotating asymptotic regime. *Journal of Fluid Mechanics* 813, 558–593 (2017). doi:10.1017/jfm.2016.789

* *BHF18*:
    Barrois, O., Hammer, M. D., Finlay, C. C., Martin, Y. & Gillet, N. Assimilation of ground and satellite magnetic measurements: inference of core surface magnetic and velocity field changes. Geophysical Journal International (2018). doi:10.1093/gji/ggy297

* *FKO20*:
    Finlay, C.C., Kloss, C., Olsen, N., Hammer, M. Toeffner-Clausen, L., Grayver, A and Kuvshinov, A. (2020), The CHAOS-7 geomagnetic field model and observed changes in the South Atlantic Anomaly, Earth Planets and Space 72, doi:10.1186/s40623-020-01252-9

* *GJF13*:
    Gillet, N., Jault, D., Finlay, C. C. & Olsen, N. Stochastic modeling of the Earth’s magnetic field: Inversion for covariances over the observatory era. *Geochemistry, Geophysics, Geosystems* 14, 766–786 (2013). doi:10.1002/ggge.20041

* *GBF15*:
    Gillet, N., Barrois, O. & Finlay, C. C. Stochastic forecasting of the geomagnetic field from the COV-OBS.x1 geomagnetic field model, and candidate models for IGRF-12. *Earth, Planets and Space* 67, (2015). doi:10.1186/s40623-015-0225-z

* *Hammer18*:
    Hammer, M. D. Local Estimation of the Earth’s Core Magnetic Field. (Technical University of Denmark (DTU), 2018).

* *Hammer2021a*
	Hammer, M. D., Cox, G. A., Brown, W. J., Beggan, C. D., & Finlay, C. C., 2021a. Geomagnetic virtual observatories: monitoring geomagnetic secular variation with the swarm satellites, Earth Planets and Space, 73(1), 1–22

* *Hammer2021b*
	Hammer, M. D., Finlay, C. C., & Olsen, N., 2021b. Applications for cryosat-2 satellite magnetic data in studies of earth’s core field variations, Earth, Planets and Space, 73(1), 1–22

* *JMS22* 
    Julien, B., Matthias, H., Saynisch-Wagner, J. et al. Kalmag: a high spatio-temporal model of the geomagnetic field. Earth Planets Space 74, 139 (2022). https://doi.org/10.1186/s40623-022-01692-5
    
* *cdflib*
	https://pypi.org/project/cdflib/
